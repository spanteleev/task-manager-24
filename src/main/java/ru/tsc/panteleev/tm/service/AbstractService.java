package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.api.service.IService;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.field.IdEmptyException;
import ru.tsc.panteleev.tm.exception.field.IndexIncorrectException;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    public AbstractService(@Nullable final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Optional<M> model = Optional.ofNullable(repository.findById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M findByIndex(@Nullable final Integer index) {
        if (index<0 || index > repository.getSize()) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index<0 || index > repository.getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }
}
