package ru.tsc.panteleev.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateBegin {

    @Nullable
    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}
